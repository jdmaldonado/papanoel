﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nancy;
using Nancy.ModelBinding;
using PapaNoel.DataBase;
using PapaNoel.Models;
using PapaNoel.Services;
using System.Globalization;
using Nancy.Responses;

namespace PapaNoel.Modules
{
    public class ClientsModule : NancyModule
    {
        public ClientsModule( ClientService service)
            : base("Clients")
        {
            Get["/"] = parameters =>
            {
                List<Client> clients = service.SetOperatorNameForGet();

                return clients;
            };

            Get["/CallStatus/{Identification}"] = parameters =>
            {
                var clientIdentification = (long)parameters.Identification;

                List<Client> _clients = new List<Client>();

                Client _client = DbManager.GetCallStatusByClientId(clientIdentification);

                if (_client == null)
                {
                    return _clients;
                }
                else
                {
                    _clients.Add(_client);

                    return _clients;
                }
            };

            Get["/{Identification}"] = parameters =>
            {
                var clientIdentification = (long)parameters.Identification;

                List<Client> _clients  = new List<Client>();
                
                Client _client = DbManager.GetClientByIdentification(clientIdentification);

                if (_client == null)
                {
                    return _clients;
                }
                else
                {
                    _clients.Add(_client);
                    Operator _operator = _clients[0].LlamadoPor == null ? null : DbManager.GetOperatorFromId(_client.LlamadoPor.Value);

                    _clients[0].Asesor = _operator == null ? null : _operator.Nombres + " " + _operator.Apellidos;

                    return _clients;
                }               
            };

            Get["/Historical"] = parameters =>
            {
                DateTime? initialTypeDate, finalTypeDate; //Digitación
                DateTime? initialCallDate, finalCallDate; //Llamada

                long firstTypeDate, lastTypeDate;
                long firstCallDate, lastCallDate;

                Boolean status, typeClient;
                Boolean? estado, tipoCliente;
                var format = (string)Request.Query.format;

                //estado Cliente
                if (Boolean.TryParse(Request.Query.Status, out status))
                    estado = status;
                else
                    estado = null;

                //Tipo CLiente
                if (Boolean.TryParse(Request.Query.ClientType, out typeClient))
                    tipoCliente = typeClient;
                else
                    tipoCliente = null;


                /* Type DateTime*/
                if (long.TryParse(Request.Query.startTypeDate, out firstTypeDate))
                    initialTypeDate = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(firstTypeDate);
                else
                    initialTypeDate = null;

                if (long.TryParse(Request.Query.finalTypeDate, out lastTypeDate))
                    finalTypeDate = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(lastTypeDate);
                else
                    finalTypeDate = null;

                /* Call DateTime*/
                if (long.TryParse(Request.Query.startCallDate, out firstCallDate))
                    initialCallDate = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(firstCallDate);
                else
                    initialCallDate = null;

                if (long.TryParse(Request.Query.finalCallDate, out lastCallDate))
                    finalCallDate = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddMilliseconds(lastCallDate);
                else
                    finalCallDate = null;

                List<Client> clients = service.SetNamesForGrid(DbManager.GetClientsForGrid(estado, tipoCliente, initialTypeDate, finalTypeDate, initialCallDate, finalCallDate ));

                if (string.Compare(format, "spreadsheet") == 0)
                {
                    var culture = new CultureInfo("es-CO");

                    var file = service.GenerateExcelFileForClients(clients, culture);
                    var response = new StreamResponse(() => file, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    return response.AsAttachment("HistoricClients.xlsx");
                }
                return clients;
            };

            Post["/create/{operatorCode}"] = parameters =>
            {
                var operatorCode = (long)parameters.operatorCode;
                var client = this.Bind<Client>();

                Client createdClient = service.SetOperatorIdForCreation(client, operatorCode);

                return createdClient;
            };

            Post["/createReferred/{operatorCode}"] = parameters =>
            {
                var operatorCode = (long)parameters.operatorCode;

                var client = this.Bind<Client>();

                Client createdClient = service.SetOperatorIdForReferredCreation(client, operatorCode);

                return createdClient;
            };

            Put["/update/{IdClient}/{operatorCode}"] = parameters =>
            {
                var operatorCode = (long)parameters.operatorCode;
                var client = this.Bind<Client>();

                var status = service.SetOperatorIdForUpdate(client, operatorCode);


                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Put["/update/call/{operatorCode}"] = parameters =>
            {
                var operatorCode = (long)parameters.operatorCode;
                var client = this.Bind<Client>();

                var status = service.SetOperatorIdForCall(client, operatorCode);


                return new Response().WithStatusCode(HttpStatusCode.OK);
            };

            Put["/update/CallStatus/{operatorCode}"] = parameters =>
            {
                var operatorCode = (long)parameters.operatorCode;
                var client = this.Bind<Client>();

                var status = service.SetOperatorIdForCallStatus(client, operatorCode);


                return new Response().WithStatusCode(HttpStatusCode.OK);
            };
        }
    }
}