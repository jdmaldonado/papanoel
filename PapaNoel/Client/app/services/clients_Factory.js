﻿PapaNoelApp.factory('clients_Factory', ['$resource', function ($resource) {

    return {
        clients:
            $resource('Clients', { },
                {
                    "GetClients": { method: "GET", params: { }, isArray: true }
                }
            ),
        client:
            $resource('Clients/:identification', { identification: '@identification' },
                {
                    "GetByIdentification": { method: "GET", params: { identification: '@identification' }, isArray: true }
                }
            ),
        clientCall:
            $resource('Clients/CallStatus/:identification', { identification: '@identification' },
                {
                    "GetCallStatusByIdentification": { method: "GET", params: { identification: '@identification' }, isArray: true }
                }
            ),
        clientsGrid:
            $resource('Clients/Historical', {}),

        createClient:
            $resource('Clients/Create/:operatorCode', { operatorCode: '@operatorCode' },
                {
                    "PostClient": { method: "POST", params: { operatorCode: '@operatorCode'}, isArray: false }
                }
            ),
        updateClient:
             $resource('Clients/update/:idClient/:operatorCode', { idClient: '@idClient', operatorCode: '@operatorCode' },
                {
                    "putClient": { method: "PUT", params: { idClient: '@idClient', operatorCode: '@operatorCode' }, isArray: false }
                }
            ),
        callClient:
             $resource('Clients/update/call/:operatorCode', { operatorCode: '@operatorCode' },
                {
                    "call": { method: "PUT", params: { operatorCode: '@operatorCode' }, isArray: false }
                }
            ),
        updateStatus:
            $resource('Clients/update/CallStatus/:operatorCode', { operatorCode: '@operatorCode' },
                {
                    "callStatus": { method: "PUT", params: { operatorCode: '@operatorCode' }, isArray: false }
                }
            ),
        createReferred:
            $resource('Clients/createReferred/:operatorCode', { operatorCode: '@operatorCode' },
                {
                    "PostReferred": { method: "POST", params: { operatorCode: '@operatorCode'}, isArray: false }
                }
            )
    };

}]);