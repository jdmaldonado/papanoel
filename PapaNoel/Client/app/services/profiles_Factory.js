﻿PapaNoelApp.factory('profiles_Factory', ['$resource', function ($resource) {

    return {
        profiles:
            $resource('Profiles', {},
                {
                    "GetProfiles": { method: "GET", params: {}, isArray: true }
                }
            ),
        updateProfile:
             $resource('Profiles/update/:idPerfil', { idPerfil: '@idPerfil' },
                {
                    "putProfile": { method: "PUT", params: { idPerfil: '@idPerfil' }, isArray: false }
                }
            ),
        createProfile:
             $resource('Profiles/create', {},
                {
                    "postProfile": { method: "POST", params: {}, isArray: false }
                }
            )
    };

}]);