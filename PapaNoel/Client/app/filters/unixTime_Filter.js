﻿PapaNoelApp.filter('unixTime_Filter', function () {
    return function (input) {
        if (!input)
            return null;
        return moment(input).valueOf();
    };
});