﻿PapaNoelApp.directive('buttonBack', [function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            label: '@',
            code: '@'
        },
        template: '<a ng-href="#/menu/{{code}}" id="btnGoBack" class="btn btn-block" role="button">' +
                        '<span class="glyphicon glyphicon-circle-arrow-left"></span>' +
                        ' {{ label }}' +
                    '</a>',
        link: function (scope, element, attrs) {
            if (attrs.background) {
                element[0].style.backgroundColor = attrs.background;
                element.on('mouseenter', function () {
                    element[0].style.backgroundColor = attrs.backgroundHover;
                });
                element.on('mouseleave', function () {
                    element[0].style.backgroundColor = attrs.background;
                });
            } else {
                element[0].style.backgroundColor = '#546e7a';
                element.on('mouseenter', function () {
                    element[0].style.backgroundColor = '#78909c';
                });
                element.on('mouseleave', function () {
                    element[0].style.backgroundColor = '#546e7a';
                });
            }
            element[0].style.color = attrs.color;
            element[0].style.fontSize = attrs.fontsize;
        }
    }
}
])