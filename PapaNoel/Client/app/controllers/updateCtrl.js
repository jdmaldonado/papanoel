﻿PapaNoelApp.controller('updateCtrl', function updateCtrl($scope, $rootScope, $routeParams, clients_Factory) {

    /* Este controlador debe recibir el código del Operador para que pueda funcionar correctamente */
    $scope.operatorCode = $routeParams.operatorCode;

    $scope.view = {
        editInfo: true,
        form: false
    };

    $scope.noRegistredClient = false;
    $scope.notCalledClient = false;
    $scope.sameStatus = false;
        
    $scope.updateForm = null; /* Formulario Básico */
    

    /* Limpia Los formularios */
    $scope.Clean = function () {
        $scope.view.editInfo = true;
        $scope.view.form = false;
        $scope.identification = undefined;
    }

    /* Actualiza el formulario principal con la información del cliente seleccionado */
    $scope.Validate = function (identification) {

        $scope.updateForm = null;
        $scope.noRegistredClient = false;
        $scope.sameStatus = false;
        $scope.notCalledClient = false;

        /* Primero Esconda el formulario de edición y muestre el básico */
        $scope.view.editInfo = false;
        $scope.view.form = true;
        

        /*Traemos*/
        clients_Factory.clientCall.GetCallStatusByIdentification({ identification: identification }).$promise.then(function (data) {
            if (data.length > 0) {
                $scope.updateForm = data[0];
            }

            /*Si el cliente Existe*/
            if ($scope.updateForm != null) {

                /* Si el cliente Existe y ha sido llamado*/
                if ($scope.updateForm.estado == true) {

                    $scope.notCalledClient = false;
                }
                    /* Si el cliente Existe y NO ha sido llamado*/
                else {
                    $scope.notCalledClient = true;
                }
            }
                /* Si el cliente No Existe */
            else {
                $scope.notCalledClient = false;
                $scope.noRegistredClient = true;
            }

        });

    }

    $scope.Save = function () {

        if ($scope.updateForm.estadoActualLlamada == $scope.updateForm.estadoLlamada) {
            $scope.sameStatus = true;
            $scope.$apply();
        }
        else {
            if ($scope.updateForm.idCliente === undefined) {
                alert("Hay Campos sin diligenciar");
            }
            else {
                clients_Factory.updateStatus.callStatus({ idClient: $scope.updateForm.idCliente, operatorCode: $scope.operatorCode }, $scope.updateForm);
            }
        }

        $scope.Clean();

    }

    
});

