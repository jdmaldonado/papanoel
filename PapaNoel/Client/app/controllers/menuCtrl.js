﻿PapaNoelApp.controller('menuCtrl', function menuCtrl($scope, $rootScope, $routeParams, menus_Factory) {

    $scope.menus = {};
    $scope.operatorCode = $routeParams.operatorCode;

    menus_Factory.menus.GetMenus({ operatorCode: $scope.operatorCode }).$promise.then(function (data) {
        //getCurrentUser(data);
        $scope.menus = data;
    });


    //var getCurrentUser = function (data) {
    //    $scope.usuarioActual = data['Activos'][0].nombre;
    //}
});

