﻿PapaNoelApp.controller('operatorCtrl', function operatorCtrl($scope, $rootScope, $routeParams, operators_Factory, profiles_Factory) {

    /* Este controlador debe recibir el código del Operador para que pueda funcionar correctamente */
    $scope.operatorCode = $routeParams.operatorCode;


    $scope.view = {
        editInfo: false,
        form: true
    };

    $scope.profile = {};
    $scope.processing = true;
    $scope.formOperator = {};

    getOperators = function () {
        $scope.operadores = operators_Factory.operators.GetOperators({});
        $scope.processing = false;
    }

    $scope.perfiles = profiles_Factory.profiles.GetProfiles({});

    getOperators();

    $scope.loadEditForm = function () {
        $scope.view.editInfo = true;
        $scope.view.form = false;
        $scope.operator = undefined;
        $scope.profile = undefined;
    }

    $scope.Update = function (operatorName) {

        $scope.view.form = true;
        $scope.view.editInfo = false;


        $scope.operadores.map(function (objeto) {
            if (angular.equals(objeto.nombres, operatorName)) {
                $scope.formOperator = objeto;
            }
        })

        $scope.perfiles.map(function (objeto) {
            if (objeto.idPerfil == $scope.formOperator.idPerfil) {
                $scope.profile = objeto;
            }
        })
    }

    $scope.Limpiar = function () {
        $scope.formOperator = {};
        $scope.profile = {};
        $scope.view.editInfo = false;
        $scope.view.form = true;
        getOperators();
    }

    $scope.Guardar = function () {

        $scope.formOperator.idPerfil = $scope.profile.idPerfil;

        if ($scope.formOperator.idOperador === undefined) {

            operators_Factory.createOperator.postOperator({}, $scope.formOperator).$promise.then(function () {
                $scope.Limpiar();
            });
        }
        else {
            operators_Factory.updateOperator.putOperator({ idOperador: $scope.formOperator.idOperador }, $scope.formOperator).$promise.then(function () {
                $scope.Limpiar();
            });
        }

    }

});

