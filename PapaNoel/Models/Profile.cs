﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PapaNoel.Models
{
    public class Profile
    {
        public int IdPerfil { get; set; }
        public string Perfil { get; set; }
    }
}